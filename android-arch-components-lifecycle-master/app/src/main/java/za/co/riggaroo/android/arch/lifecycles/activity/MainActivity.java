package za.co.riggaroo.android.arch.lifecycles.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.squareup.picasso.Picasso;

import java.util.Date;

import za.co.riggaroo.android.arch.lifecycles.R;
import za.co.riggaroo.android.arch.lifecycles.fragment.Fragment01;
import za.co.riggaroo.android.arch.lifecycles.fragment.Fragment05;
import za.co.riggaroo.android.arch.lifecycles.fragment.Fragment09;
import za.co.riggaroo.android.arch.lifecycles.fragment.Fragment10;
import za.co.riggaroo.android.arch.lifecycles.fragment.fragment06;

public class MainActivity extends FragmentActivity implements View.OnClickListener {
    private final String TAG = " MainActivityTAG - ";

    private int mCurrentFragmentIndex;
    public final static int FRAGMENT_ONE = 0;
    public final static int FRAGMENT_TWO = 1;
    public final static int FRAGMENT_THREE = 2;
    public final static int FRAGMENT_FOUR = 3;
    public final static int FRAGMENT_FIVE = 4;
    public final static int FRAGMENT_SIX = 5;
    public final static int FRAGMENT_SEVEN = 6;
    public final static int FRAGMENT_EIGHT = 7;
    public final static int FRAGMENT_NINE = 8;
    public final static int FRAGMENT_TEN = 9;
    public final static int FRAGMENT_ELEVEN = 10;
    public final static int FRAGMENT_TWELVE = 11;
    public final static int FRAGMENT_THIRTEEN = 12;
    public final static int FRAGMENT_FOURTEEN = 13;
    public final static int FRAGMENT_FIFTEEN = 14;
    public final static int FRAGMENT_SIXTEEN = 15;
    public final static int FRAGMENT_SEVENTEEN = 16;
    public final static int FRAGMENT_HUNDRED = 99;

    private TextView tv_fragment01;
    private TextView tv_fragment02;
    private TextView tv_fragment03;
    private TextView tv_fragment04;
    private TextView tv_fragment05;
    private TextView tv_fragment06;
    private TextView tv_fragment07;
    private TextView tv_fragment08;
    private TextView tv_fragment09;
    private TextView tv_fragment10;
    private TextView tv_fragment11;
    private TextView tv_fragment12;
    private TextView tv_fragment13;
    private TextView tv_fragment14;
    private TextView tv_fragment15;
    private TextView tv_fragment16;
    private TextView tv_fragment17;
    private TextView tv_fragment100;

    private String fragment01Url = "";
    private String fragment02Url = "";
    private String fragment03Url = "";
    private String fragment04Url = "";
    private String fragment05Url = "";
    private String fragment06Url = "";
    private String fragment07Url = "";
    private String fragment08Url = "";
    private String fragment09Url = "";
    private String fragment10Url = "";
    private String fragment11Url = "";
    private String fragment12Url = "";
    private String fragment13Url = "";
    private String fragment14Url = "";
    private String fragment15Url = "";
    private String fragment16Url = "";
    private String fragment17Url = "";
    private String fragment100Url = "";

    private String nextAppUrl = "";
    private String mxPlayerUrl = "";
    private Date curDate;
    private String appStatus = "99";

    private int adsCnt = 0;
    InterstitialAd interstitialAd;
    InterstitialAd interstitialAd3;

    Date d1;
    Date d2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        Intent mainIntent = getIntent();

        appStatus = mainIntent.getStringExtra("appStatus");
        Log.d(TAG, "appStatus : " + appStatus);
        d1 = new Date();

        SharedPreferences pref= getSharedPreferences("pref", MODE_PRIVATE); // 선언
        String first = pref.getString("first",null);
        if(first != null && !appStatus.equals("3") && !appStatus.equals("4"))  appStatus = "1";

        if(appStatus.equals("1")){
            setContentView(R.layout.activity_main);

            curDate = new Date();

            AdView adView = (AdView) findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            adView.loadAd(adRequest);

            AdRequest adRequest2 = new AdRequest.Builder().build();
            interstitialAd = new InterstitialAd(this);
            interstitialAd.setAdUnitId(getResources().getString(R.string.full_mid));
            interstitialAd.loadAd(adRequest2);

            tv_fragment01 = (TextView)findViewById(R.id.tv_fragment01);
            tv_fragment02 = (TextView)findViewById(R.id.tv_fragment02);
            tv_fragment03 = (TextView)findViewById(R.id.tv_fragment03);
            tv_fragment04 = (TextView)findViewById(R.id.tv_fragment04);
            tv_fragment05 = (TextView)findViewById(R.id.tv_fragment05);
            tv_fragment06 = (TextView)findViewById(R.id.tv_fragment06);
            tv_fragment07 = (TextView)findViewById(R.id.tv_fragment07);
            tv_fragment08 = (TextView)findViewById(R.id.tv_fragment08);
            tv_fragment09 = (TextView)findViewById(R.id.tv_fragment09);
            tv_fragment10 = (TextView)findViewById(R.id.tv_fragment10);
            tv_fragment11 = (TextView)findViewById(R.id.tv_fragment11);
            tv_fragment12 = (TextView)findViewById(R.id.tv_fragment12);
            tv_fragment13 = (TextView)findViewById(R.id.tv_fragment13);
            tv_fragment14 = (TextView)findViewById(R.id.tv_fragment14);
            //tv_fragment15 = (TextView)findViewById(R.id.tv_fragment15);
            //tv_fragment16 = (TextView)findViewById(R.id.tv_fragment16);
            //tv_fragment17 = (TextView)findViewById(R.id.tv_fragment17);
            //tv_fragment100 = (TextView)findViewById(R.id.tv_fragment100);

            tv_fragment01.setOnClickListener(this);
            tv_fragment02.setOnClickListener(this);
            tv_fragment03.setOnClickListener(this);
            tv_fragment04.setOnClickListener(this);
            tv_fragment05.setOnClickListener(this);
            tv_fragment06.setOnClickListener(this);
            tv_fragment07.setOnClickListener(this);
            tv_fragment08.setOnClickListener(this);
            tv_fragment09.setOnClickListener(this);
            tv_fragment10.setOnClickListener(this);
            tv_fragment11.setOnClickListener(this);
            tv_fragment12.setOnClickListener(this);
            tv_fragment13.setOnClickListener(this);
            tv_fragment14.setOnClickListener(this);
            //tv_fragment15.setOnClickListener(this);
            //tv_fragment16.setOnClickListener(this);
            //tv_fragment17.setOnClickListener(this);
            //tv_fragment100.setOnClickListener(this);

            fragment01Url = mainIntent.getStringExtra("fragment01Url");
            fragment02Url = mainIntent.getStringExtra("fragment02Url");
            fragment03Url = mainIntent.getStringExtra("fragment03Url");
            fragment04Url = mainIntent.getStringExtra("fragment04Url");
            fragment05Url = mainIntent.getStringExtra("fragment05Url");
            fragment06Url = mainIntent.getStringExtra("fragment06Url");
            fragment07Url = mainIntent.getStringExtra("fragment07Url");
            fragment08Url = mainIntent.getStringExtra("fragment08Url");
            fragment09Url = mainIntent.getStringExtra("fragment09Url");
            fragment10Url = mainIntent.getStringExtra("fragment10Url");
            fragment11Url = mainIntent.getStringExtra("fragment11Url");
            fragment12Url = mainIntent.getStringExtra("fragment12Url");
            fragment13Url = mainIntent.getStringExtra("fragment13Url");
            fragment14Url = mainIntent.getStringExtra("fragment14Url");
            //fragment15Url = mainIntent.getStringExtra("fragment15Url");
            //fragment16Url = mainIntent.getStringExtra("fragment16Url");
            //fragment17Url = mainIntent.getStringExtra("fragment17Url");
            //fragment100Url = mainIntent.getStringExtra("fragment100Url");

            mCurrentFragmentIndex = FRAGMENT_ONE;     // 첫 Fragment 를 초기화
            fragmentReplace(mCurrentFragmentIndex);

            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
            editor.putString("first", "1"); //First라는 key값으로 id 데이터를 저장한다.
            editor.putString("adsCnt", "1"); //First라는 key값으로 id 데이터를 저장한다.
            editor.commit(); //완료한다.

        } else if(appStatus.equals("2")){
            setContentView(R.layout.maintenance);
            String maintenanceImgUrl = mainIntent.getStringExtra("maintenance");
            ImageView imgView = (ImageView)findViewById(R.id.img_maintenance);
            if(maintenanceImgUrl == null || maintenanceImgUrl.equals("")){
                imgView.setImageResource(R.drawable.noimage);
            } else {
                Picasso.with(this).load(maintenanceImgUrl).into(imgView);
            }
        } else if(appStatus.equals("3")){
            setContentView(R.layout.closed);
            String closedImgUrl = mainIntent.getStringExtra("closed");
            ImageView imgView = (ImageView)findViewById(R.id.img_closed);
            nextAppUrl = mainIntent.getStringExtra("nextAppUrl");
            if(closedImgUrl == null || closedImgUrl.equals("")){
                imgView.setImageResource(R.drawable.noimage);
            } else {
                Picasso.with(this).load(closedImgUrl).into(imgView);
            }
            Button btnClosed = (Button) findViewById(R.id.btn_closed);
            btnClosed.setOnClickListener(this);
        } else if(appStatus.equals("4")) {   // mx player
            setContentView(R.layout.mxplayer);
            mxPlayerUrl = mainIntent.getStringExtra("mxPlayerUrl");
            Button btnMxPlayer = (Button) findViewById(R.id.btn_mxplayer);
            btnMxPlayer.setOnClickListener(this);
        } else if(appStatus.equals("9")) {   // mx player
            setContentView(R.layout.closed);
            String closedImgUrl = mainIntent.getStringExtra("closedImgUrl");
            nextAppUrl = mainIntent.getStringExtra("nextAppUrl");
            Log.d(TAG, "  " + nextAppUrl);
            ImageView imgView = (ImageView)findViewById(R.id.img_closed);
            Picasso.with(this).load(closedImgUrl).into(imgView);
            Button btnClosed = (Button) findViewById(R.id.btn_closed);
            btnClosed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent marketLaunch1 = new Intent(Intent.ACTION_VIEW);
                    marketLaunch1.setData(Uri.parse(nextAppUrl));
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(nextAppUrl));
                    startActivity(intent);
                }
            });
        }

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.tv_fragment01:
                offColorTv();
                tv_fragment01.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_ONE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment02:
                offColorTv();
                tv_fragment02.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWO;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment03:
                offColorTv();
                tv_fragment03.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_THREE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment04:
                offColorTv();
                tv_fragment04.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_FOUR;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment05:
                offColorTv();
                tv_fragment05.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_FIVE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment06:
                offColorTv();
                tv_fragment06.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_SIX;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment07:
                offColorTv();
                tv_fragment07.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_SEVEN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment08:
                offColorTv();
                tv_fragment08.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_EIGHT;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment09:
                offColorTv();
                tv_fragment09.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_NINE;
                fragmentReplace(mCurrentFragmentIndex);
                break;

            case R.id.tv_fragment10:
                offColorTv();
                tv_fragment10.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TEN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment11:
                offColorTv();
                tv_fragment11.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_ELEVEN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment12:
                offColorTv();
                tv_fragment12.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWELVE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment13:
                offColorTv();
                tv_fragment13.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_THIRTEEN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment14:
                offColorTv();
                tv_fragment14.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_FOURTEEN;
                fragmentReplace(mCurrentFragmentIndex);
                break;

            case R.id.btn_closed:
                Intent marketLaunch1 = new Intent(Intent.ACTION_VIEW);
                Log.d(TAG, "  " + nextAppUrl);
                marketLaunch1.setData(Uri.parse(nextAppUrl));
                startActivity(marketLaunch1);
                break;
            case R.id.btn_mxplayer:
                Intent marketLaunch2 = new Intent(Intent.ACTION_VIEW);
                marketLaunch2.setData(Uri.parse(mxPlayerUrl));
                startActivity(marketLaunch2);
                break;
        }
    }

    private Fragment getFragment(int idx) {
        Fragment newFragment = null;
        Bundle args = new Bundle();
        args.putInt("adsCnt", adsCnt);

        switch (idx) {
            case FRAGMENT_ONE:
                newFragment = new Fragment01();
                args.putString("baseUrl", fragment01Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWO:
                newFragment = new Fragment01();
                args.putString("baseUrl", fragment02Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_THREE:
                newFragment = new Fragment01();
                args.putString("baseUrl", fragment03Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FOUR:
                newFragment = new Fragment01();
                args.putString("baseUrl", fragment04Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FIVE:
                newFragment = new Fragment05();
                args.putString("baseUrl", fragment05Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_SIX:
                newFragment = new fragment06();
                args.putString("baseUrl", fragment06Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_SEVEN:
                newFragment = new fragment06();
                args.putString("baseUrl", fragment07Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_EIGHT:
                newFragment = new fragment06();
                args.putString("baseUrl", fragment08Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_NINE:
                newFragment = new Fragment09();
                args.putString("baseUrl", fragment09Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TEN:
                newFragment = new Fragment10();
                args.putString("baseUrl", fragment10Url);
                args.putString("baseEndUrl", "%7C6/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_ELEVEN:
                newFragment = new Fragment09();
                args.putString("baseUrl", fragment11Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWELVE:
                newFragment = new Fragment10();
                args.putString("baseUrl", fragment12Url);
                args.putString("baseEndUrl", "%7C7/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_THIRTEEN:
                newFragment = new Fragment09();
                args.putString("baseUrl", fragment13Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FOURTEEN:
                newFragment = new Fragment10();
                args.putString("baseUrl", fragment14Url);
                args.putString("baseEndUrl", "%7C8/page/");
                newFragment.setArguments(args);
                break;

            default:
                Log.d(TAG, "Unhandle case");
                newFragment = new Fragment01();
                args.putString("baseUrl", fragment01Url);
                newFragment.setArguments(args);
                break;
        }

        return newFragment;
    }

    public void fragmentReplace(int reqNewFragmentIndex) {
        Fragment newFragment = null;
        Log.d(TAG, "fragmentReplace " + reqNewFragmentIndex);
        newFragment = getFragment(reqNewFragmentIndex);
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction(); // replace fragment
        transaction.replace(R.id.ll_fragment, newFragment);
        transaction.commit();   // Commit the transaction
    }

    public void offColorTv(){
        tv_fragment01.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment02.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment03.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment04.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment05.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment06.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment07.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment08.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment09.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment10.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment11.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment12.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment13.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment14.setBackgroundResource(R.drawable.fragment_borther);

    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d(TAG, "main activity onResume");

        if(appStatus.equals("1") && d1 != null){
            d2 = new Date();
            long diff = d2.getTime() - d1.getTime();
            long min = diff / 1000 / 60 ;
            Log.d(TAG, "min : " + min);
            if(min >= 30){
                d1 = new Date();
                AdRequest adRequest3 = new AdRequest.Builder().build();
                interstitialAd3 = new InterstitialAd(this);
                interstitialAd3.setAdUnitId(getResources().getString(R.string.full_end));
                interstitialAd3.loadAd(adRequest3);
                interstitialAd3.setAdListener(new AdListener(){
                    @Override public void onAdLoaded() {
                        if (interstitialAd3.isLoaded()) {
                            interstitialAd3.show();
                        }
                    }
                });
            }
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this).setTitle("안녕히 가세요.")
                .setMessage("종료 하시겠습니까?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // 확인시 처리 로직
                        if(appStatus.equals("1")){
                            interstitialAd.show();
                        }
                        finish();
                    }})
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // 취소시 처리 로직
                        return;
                    }})
                .show();
    }
}
