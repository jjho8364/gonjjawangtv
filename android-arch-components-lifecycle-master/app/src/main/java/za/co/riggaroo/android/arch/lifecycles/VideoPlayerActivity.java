package za.co.riggaroo.android.arch.lifecycles;

import android.arch.lifecycle.LifecycleActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;

/**
 * @author rebeccafranks
 * @since 2017/07/13.
 */

public class VideoPlayerActivity extends LifecycleActivity {
    private static String TAG = "VideoPlayerActivity";
    private String fileType = "";
    private String videoUrl = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_video_palyer);
        SimpleExoPlayerView playerView = findViewById(R.id.simple_exoplayer_view);

        fileType = getIntent().getStringExtra("fileType");
        videoUrl = getIntent().getStringExtra("videoUrl");


        ImageView loadingImg = (ImageView)findViewById(R.id.gif_load);
        GlideDrawableImageViewTarget gifImage = new GlideDrawableImageViewTarget(loadingImg);
        Glide.with(this).load(R.drawable.loding).into(gifImage);

        getLifecycle().addObserver(new VideoPlayerComponent(getApplicationContext(), playerView, fileType, videoUrl, loadingImg));

    }
}
