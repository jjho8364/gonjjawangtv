package za.co.riggaroo.android.arch.lifecycles.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import za.co.riggaroo.android.arch.lifecycles.R;
import za.co.riggaroo.android.arch.lifecycles.VideoPlayerActivity;

public class SplashActivity extends Activity {
    private String TAG = " SplashActivity - ";
    private final String baseUrl = "https://freekinglivetv.wordpress.com/livetvindo1/";
    private GetStatus getStatus = null;
    private String splashInfo3 = "";

    private TextView tvSplashInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        tvSplashInfo = (TextView)findViewById(R.id.splash_info3);

        getStatus = new GetStatus();
        getStatus.execute();
    }

    public class GetStatus extends AsyncTask<Void, Void, Void> {

        String appStatus = "";
        String fragment01Url = "";
        String fragment02Url = "";
        String fragment03Url = "";
        String fragment04Url = "";
        String fragment05Url = "";
        String fragment06Url = "";
        String fragment07Url = "";
        String fragment08Url = "";
        String fragment09Url = "";
        String fragment10Url = "";
        String fragment11Url = "";
        String fragment12Url = "";
        String fragment13Url = "";
        String fragment14Url = "";
        String fragment15Url = "";
        String fragment16Url = "";
        String fragment17Url = "";
        String fragment100Url = "";

        String maintenanceImgUrl = "";
        String closedImgUrl = "";
        String nextAppUrl = "";
        String mxPlayerUrl = "";
        String hideFragment = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;

            try {
                doc = Jsoup.connect(baseUrl).timeout(15000).get();

                appStatus = doc.select(".status.real").text();
                Log.d(TAG, "appStatus : " + appStatus);

                splashInfo3 = doc.select(".splash_info3").text();

                if(appStatus.equals("1")){
                    fragment01Url = doc.select(".fragment01url").text();
                    fragment02Url = doc.select(".fragment02url").text();
                    fragment03Url = doc.select(".fragment03url").text();
                    fragment04Url = doc.select(".fragment04url").text();
                    fragment05Url = doc.select(".fragment05url").text();
                    fragment06Url = doc.select(".fragment06url").text();
                    fragment07Url = doc.select(".fragment07url").text();
                    fragment08Url = doc.select(".fragment08url").text();
                    fragment09Url = doc.select(".fragment09url").text();
                    fragment10Url = doc.select(".fragment10url").text();
                    fragment11Url = doc.select(".fragment11url").text();
                    fragment12Url = doc.select(".fragment12url").text();
                    fragment13Url = doc.select(".fragment13url").text();
                    fragment14Url = doc.select(".fragment14url").text();
                    fragment15Url = doc.select(".fragment15url").text();
                    fragment16Url = doc.select(".fragment16url").text();
                    fragment17Url = doc.select(".fragment17url").text();
                    fragment100Url = doc.select(".fragment100url").text();
                    mxPlayerUrl = doc.select(".mxplayer").text();
                    hideFragment = doc.select(".hidefragment").text();

                    Intent startLink1 = getPackageManager().getLaunchIntentForPackage("com.mxtech.videoplayer.ad");
                    Intent startLink2 = getPackageManager().getLaunchIntentForPackage("com.mxtech.videoplayer.pro");
                    if(startLink1 == null && startLink2 == null){
                        //appStatus = "4";
                        mxPlayerUrl = doc.select(".mxplayer").text();
                    }
                } else if(appStatus.equals("2")){
                    maintenanceImgUrl = doc.select(".maintenance").text();
                    fragment01Url = doc.select(".fragment01url").text();
                    fragment02Url = doc.select(".fragment02url").text();
                    fragment03Url = doc.select(".fragment03url").text();
                    fragment04Url = doc.select(".fragment04url").text();
                    fragment05Url = doc.select(".fragment05url").text();
                    fragment06Url = doc.select(".fragment06url").text();
                    fragment07Url = doc.select(".fragment07url").text();
                    fragment08Url = doc.select(".fragment08url").text();
                    fragment09Url = doc.select(".fragment09url").text();
                    fragment10Url = doc.select(".fragment10url").text();
                    fragment11Url = doc.select(".fragment11url").text();
                    fragment12Url = doc.select(".fragment12url").text();
                    fragment13Url = doc.select(".fragment13url").text();
                    fragment14Url = doc.select(".fragment14url").text();
                    fragment15Url = doc.select(".fragment15url").text();
                    fragment16Url = doc.select(".fragment16url").text();
                    fragment17Url = doc.select(".fragment17url").text();
                    fragment100Url = doc.select(".fragment100url").text();

                    Intent startLink1 = getPackageManager().getLaunchIntentForPackage("com.mxtech.videoplayer.ad");
                    Intent startLink2 = getPackageManager().getLaunchIntentForPackage("com.mxtech.videoplayer.pro");
                    if(startLink1 == null && startLink2 == null){
                        //appStatus = "4";
                        mxPlayerUrl = doc.select(".mxplayer").text();
                    }
                    hideFragment = doc.select(".hidefragment").text();
                } else if(appStatus.equals("3")){
                    closedImgUrl = doc.select(".closed").text();
                    nextAppUrl = doc.select(".newappurl").text();
                } else if(appStatus.equals("9")){
                    closedImgUrl = doc.select(".closed").text();
                    nextAppUrl = doc.select(".mid.site").text();
                } else {
                    maintenanceImgUrl = doc.select(".maintenance").text();
                }
            } catch(Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            tvSplashInfo.setText(splashInfo3);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(getApplication(), MainActivity.class);

                    intent.putExtra("appStatus", appStatus);

                    if(appStatus.equals("1")){
                        intent.putExtra("fragment01Url", fragment01Url);
                        intent.putExtra("fragment02Url", fragment02Url);
                        intent.putExtra("fragment03Url", fragment03Url);
                        intent.putExtra("fragment04Url", fragment04Url);
                        intent.putExtra("fragment05Url", fragment05Url);
                        intent.putExtra("fragment06Url", fragment06Url);
                        intent.putExtra("fragment07Url", fragment07Url);
                        intent.putExtra("fragment08Url", fragment08Url);
                        intent.putExtra("fragment09Url", fragment09Url);
                        intent.putExtra("fragment10Url", fragment10Url);
                        intent.putExtra("fragment11Url", fragment11Url);
                        intent.putExtra("fragment12Url", fragment12Url);
                        intent.putExtra("fragment13Url", fragment13Url);
                        intent.putExtra("fragment14Url", fragment14Url);
                        intent.putExtra("fragment15Url", fragment15Url);
                        intent.putExtra("fragment16Url", fragment16Url);
                        intent.putExtra("fragment17Url", fragment17Url);
                        intent.putExtra("fragment100Url", fragment100Url);
                        intent.putExtra("hideFragment", hideFragment);
                    } else if(appStatus.equals("2")){
                        intent.putExtra("maintenance", maintenanceImgUrl);
                        intent.putExtra("fragment01Url", fragment01Url);
                        intent.putExtra("fragment02Url", fragment02Url);
                        intent.putExtra("fragment03Url", fragment03Url);
                        intent.putExtra("fragment04Url", fragment04Url);
                        intent.putExtra("fragment05Url", fragment05Url);
                        intent.putExtra("fragment06Url", fragment06Url);
                        intent.putExtra("fragment07Url", fragment07Url);
                        intent.putExtra("fragment08Url", fragment08Url);
                        intent.putExtra("fragment09Url", fragment09Url);
                        intent.putExtra("fragment10Url", fragment10Url);
                        intent.putExtra("fragment11Url", fragment11Url);
                        intent.putExtra("fragment12Url", fragment12Url);
                        intent.putExtra("fragment13Url", fragment13Url);
                        intent.putExtra("fragment14Url", fragment14Url);
                        intent.putExtra("fragment15Url", fragment15Url);
                        intent.putExtra("fragment16Url", fragment16Url);
                        intent.putExtra("fragment17Url", fragment17Url);
                        intent.putExtra("fragment100Url", fragment100Url);
                        intent.putExtra("hideFragment", hideFragment);
                    } else if(appStatus.equals("3")){
                        intent.putExtra("closed", closedImgUrl);
                        intent.putExtra("nextAppUrl", nextAppUrl);
                    } else if(appStatus.equals("4")){
                        intent.putExtra("mxPlayerUrl", mxPlayerUrl);
                    } else if(appStatus.equals("9")){
                        intent.putExtra("closed", closedImgUrl);
                        intent.putExtra("nextAppUrl", nextAppUrl);
                    } else {
                        intent.putExtra("maintenance", maintenanceImgUrl);
                    }
                    finish();
                    startActivity(intent);
                }
            }, 3000);
        }
    }
}
