package za.co.riggaroo.android.arch.lifecycles.item;

public class ListIntroItem {
    String imgUrl;
    String intro;
    String linUrl;

    public ListIntroItem(String imgUrl, String intro, String linUrl) {
        this.imgUrl = imgUrl;
        this.intro = intro;
        this.linUrl = linUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getLinUrl() {
        return linUrl;
    }

    public void setLinUrl(String linUrl) {
        this.linUrl = linUrl;
    }
}
