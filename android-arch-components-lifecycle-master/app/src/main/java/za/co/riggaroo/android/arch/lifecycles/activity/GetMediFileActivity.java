package za.co.riggaroo.android.arch.lifecycles.activity;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.MimeTypeMap;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Iterator;

import za.co.riggaroo.android.arch.lifecycles.R;
import za.co.riggaroo.android.arch.lifecycles.VideoPlayerActivity;

public class GetMediFileActivity extends Activity implements View.OnClickListener {
    private String TAG = " GetMediFileActivity - ";
    private WebView webView;
    private FrameLayout customViewContainer;
    private WebChromeClient.CustomViewCallback customViewCallback;
    private View mCustomView;
    private myWebChromeClient mWebChromeClient;
    private myWebViewClient mWebViewClient;
    private String baseUrl = "";
    private String javascript = "";
    private boolean autoCek = false;
    private String fileType = "";
    private ArrayList<String> keyArr;
    private JSONObject jsonObj;
    private GetJson getJson;

    private Button btmImergency;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_get_medi_file);

        baseUrl = getIntent().getStringExtra("baseUrl");

        btmImergency = (Button)findViewById(R.id.btn_imergency);
        btmImergency.setOnClickListener(this);

        ImageView loadingImg = (ImageView)findViewById(R.id.gif_load);
        GlideDrawableImageViewTarget gifImage = new GlideDrawableImageViewTarget(loadingImg);
        Glide.with(this).load(R.drawable.loding).into(gifImage);

        customViewContainer = (FrameLayout) findViewById(R.id.customViewContainer);
        webView = (WebView) findViewById(R.id.webView);

        webView.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "download");
                DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                dm.enqueue(request);
            }
        });

        mWebViewClient = new myWebViewClient();
        webView.setWebViewClient(mWebViewClient);

        mWebChromeClient = new myWebChromeClient();
        webView.setWebChromeClient(mWebChromeClient);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.addJavascriptInterface(new MyJavascriptInterface(), "Android");
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setSaveFormData(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);

        String newUA= "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0";
        webView.getSettings().setUserAgentString(newUA);
        webView.getSettings().setMediaPlaybackRequiresUserGesture(false);

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {


                if(url.contains(".gif") || url.contains(".png") || url.contains(".jpg")){

                } else if(javascript.contains("directshow") && url.contains(fileType)){
                    startMxPlayer(url);
                } else if(autoCek && fileType.equals("pandora.fuck") && url.contains("trans-idx.gcdn2.pandora.tv/flvemx2.pandora.tv/")){
                    startMxPlayer(url);
                } else {
                    if(autoCek && !(fileType.equals("webview")) && !(fileType.equals("html")) && url.contains(fileType)){
                        if(fileType.equals(".m3u8")){
                            if(url.contains("proxy")){
                                startMxPlayer(url.split("#")[0]);  // fucking daily motion
                            } else {
                                startMxPlayer(url);
                            }
                        } else {
                            startMxPlayer(url.split("#")[0]);  // fucking daily motion
                        }
                    } else if(autoCek && fileType.equals("hq.m3u8") && (url.contains("hd.m3u8") || url.contains("aac.m3u8")) ){
                        if(fileType.equals(".m3u8")){
                            if(url.contains("proxy")){
                                startMxPlayer(url.split("#")[0]);  // fucking daily motion
                            }
                        } else {
                            startMxPlayer(url.split("#")[0]);  // fucking daily motion
                        }
                    }
                }

                return super.shouldInterceptRequest(view, url);
            }
            /////////////////////////////////////////////////////////////////////////////////////
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i(TAG, "Processing webview url click..." + url);
                view.stopLoading();
                if(fileType.equals("html")){
                    Toast.makeText(GetMediFileActivity.this, "이 링크는 광고를 제거 할 수 없습니다.", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(baseUrl));
                    startActivity(intent);
                    finish();
                } else if(baseUrl.contains("openload")){
                    webView.loadUrl("javascript:" + javascript);
                } else if(url.contains("gdriveplayer")){
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                    finish();
                } else {
                    view.loadUrl(url);
                }
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(webView, url);
                Log.d(TAG, "page onload2");

                if(fileType.equals("html")){ // get url address
                    view.loadUrl("javascript:window.Android.getHtml(document.getElementsByTagName('body')[0].innerHTML);");
                } else if(baseUrl.contains("/videos/embed/")){
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(webView != null) webView.loadUrl("javascript:" + javascript);
                            if(webView != null) autoCek = true;
                        }
                    }, 2500 );

                } else if(!(fileType.equals("webview"))){
                    if(webView != null) webView.loadUrl("javascript:" + javascript);
                    if(webView != null) autoCek = true;
                } else if(fileType.equals("youtube")) {
                    startMxPlayer(url);
                }
            }
        });

        getJson = new GetJson();
        getJson.execute();


    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.btn_imergency :
                if(webView != null) webView.loadUrl("javascript:" + javascript);
                autoCek = true;
                break;
        }
    }

    public class GetJson extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            keyArr = new ArrayList<String>();
        }
        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;
            try {
                if(!baseUrl.contains("http")) baseUrl = "http:" + baseUrl;
                Log.d(TAG, "baseUrl : " + baseUrl) ;
                if(baseUrl.contains("k-vid")){
                    // after use jsoup execute video
                    doc = Jsoup.connect(baseUrl).timeout(20000).get();
                    Elements lists = doc.select(".list-server-items li");
                    for(int i=0 ; i<lists.size() ; i++) {
                        String localVideoUrl = lists.get(i).attr("data-video");

                        if(localVideoUrl.contains("rapidvideo") || localVideoUrl.contains("streamango")){
                            //startMxPlayer(localVideoUrl);
                            baseUrl = localVideoUrl;
                            break;
                        }
                    }
                }

                doc = Jsoup.connect("https://freekinglivetv.wordpress.com/livetvindo1/").timeout(20000).get();
                String strJson = doc.select(".json_player").text().replace("`", "\"");
                jsonObj = new JSONObject(strJson);
                Iterator<String> strKey = jsonObj.keys();
                while(strKey.hasNext()){
                    keyArr.add(strKey.next().toString());
                }
            } catch(Exception e){
                e.printStackTrace();
            }
            return null;
        }
        @Override

        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                for(int i=0 ; i<keyArr.size() ; i++){
                    if(baseUrl.contains(keyArr.get(i))){
                        String[] tempSplit = jsonObj.get(keyArr.get(i)).toString().split(",");
                        javascript = "$('" + tempSplit[0] + "').click()";
                        fileType = tempSplit[1];
                        break;
                    }
                }
            } catch(Exception e){
                e.printStackTrace();
            }

            Log.d(TAG, "fileType : " + fileType);

            if(fileType.equals("html")){
                Toast.makeText(GetMediFileActivity.this, "이 링크는 광고를 제거 할 수 없습니다.", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(baseUrl));
                startActivity(intent);
                finish();
            } else if(fileType.equals("webview")){
                Toast.makeText(GetMediFileActivity.this, "이 링크는 광고를 제거 할 수 없습니다.", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(GetMediFileActivity.this, WebviewPlayerActivity.class);
                intent.putExtra("videoUrl",baseUrl);
                startActivity(intent);
                finish();
            } else {
                webView.loadUrl(baseUrl);
            }
        }
    }

    public boolean inCustomView() {
        return (mCustomView != null);
    }

    public void hideCustomView() {
        mWebChromeClient.onHideCustomView();
    }

    @Override
    protected void onPause() {
        super.onPause();    //To change body of overridden methods use File | Settings | File Templates.
        //webView.onPause();
        if(webView != null) destroyWebView();
        if(getJson != null) getJson.cancel(true);
    }

    @Override
    protected void onResume() {
        super.onResume();    //To change body of overridden methods use File | Settings | File Templates.
        //webView.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();    //To change body of overridden methods use File | Settings | File Templates.
        if (inCustomView()) {
            hideCustomView();
        }
        if(webView != null) destroyWebView();
        if(getJson != null) getJson.cancel(true);
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (inCustomView()) {
                hideCustomView();
                return true;
            }

            if ((mCustomView == null) && webView != null && webView.canGoBack()) {
                webView.goBack();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    class myWebChromeClient extends WebChromeClient {
        private Bitmap mDefaultVideoPoster;
        private View mVideoProgressView;

        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
            onShowCustomView(view, callback);    //To change body of overridden methods use File | Settings | File Templates.
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {

            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
            mCustomView = view;
            if(webView != null){
                webView.setVisibility(View.GONE);
            }
            customViewContainer.setVisibility(View.VISIBLE);
            customViewContainer.addView(view);
            customViewCallback = callback;
        }

        @Override
        public View getVideoLoadingProgressView() {

            if (mVideoProgressView == null) {
                LayoutInflater inflater = LayoutInflater.from(GetMediFileActivity.this);
                mVideoProgressView = inflater.inflate(R.layout.video_progress, null);
            }
            return mVideoProgressView;
        }

        @Override
        public void onHideCustomView() {
            super.onHideCustomView();    //To change body of overridden methods use File | Settings | File Templates.
            if (mCustomView == null)
                return;

            if(webView != null){
                webView.setVisibility(View.VISIBLE);
            }
            customViewContainer.setVisibility(View.GONE);

            // Hide the custom view.
            mCustomView.setVisibility(View.GONE);

            // Remove the custom view from its container.
            customViewContainer.removeView(mCustomView);
            customViewCallback.onCustomViewHidden();

            mCustomView = null;
        }
    }

    class myWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);    //To change body of overridden methods use File | Settings | File Templates.
        }
    }

    public void startMxPlayer(String videoUrl){
        String mimeType = MimeTypeMap.getFileExtensionFromUrl(videoUrl);

        autoCek = false;

        Intent intent = new Intent(GetMediFileActivity.this, VideoPlayerActivity.class);
        intent.putExtra("fileType", fileType);
        intent.putExtra("videoUrl", videoUrl);
        startActivity(intent);
        finish();
    }

    public class MyJavascriptInterface {
        @JavascriptInterface
        public void getHtml(String html){
            if(html != null && !html.equals("")){
                Document doc = Jsoup.parse(html);
                String url = doc.select("video").attr("src");

                //startMxPlayer(url);
            }
        }
    }

    public void destroyWebView() {
        webView.clearHistory();
        webView.clearCache(true);
        webView.loadUrl("about:blank");
        webView.onPause();
        webView.removeAllViews();
        webView.destroyDrawingCache();
        webView.destroy();
        webView = null;
    }


}
